﻿namespace Ebay_uploads_validator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btn_ImportFiles = new System.Windows.Forms.Button();
            this.btn_CheckData = new System.Windows.Forms.Button();
            this.dgv_Preview = new System.Windows.Forms.DataGridView();
            this.dgv_NeedToFix = new System.Windows.Forms.DataGridView();
            this.btn_Export = new System.Windows.Forms.Button();
            this.lbl_Hint = new System.Windows.Forms.Label();
            this.btn_ClearItems = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.chklb_FieldsToCheck = new System.Windows.Forms.CheckedListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Preview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_NeedToFix)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.Location = new System.Drawing.Point(3, 19);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(529, 121);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_SelectedValueChanged);
            this.listBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox1_DragDrop);
            this.listBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBox1_DragEnter);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btn_ImportFiles
            // 
            this.btn_ImportFiles.Location = new System.Drawing.Point(3, 32);
            this.btn_ImportFiles.Name = "btn_ImportFiles";
            this.btn_ImportFiles.Size = new System.Drawing.Size(175, 23);
            this.btn_ImportFiles.TabIndex = 1;
            this.btn_ImportFiles.Text = "Import files or drag&&drop ->";
            this.btn_ImportFiles.UseVisualStyleBackColor = true;
            this.btn_ImportFiles.Click += new System.EventHandler(this.btn_ImportFiles_Click);
            // 
            // btn_CheckData
            // 
            this.btn_CheckData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_CheckData.Location = new System.Drawing.Point(3, 378);
            this.btn_CheckData.Name = "btn_CheckData";
            this.btn_CheckData.Size = new System.Drawing.Size(175, 23);
            this.btn_CheckData.TabIndex = 3;
            this.btn_CheckData.Text = "Check data";
            this.btn_CheckData.UseVisualStyleBackColor = true;
            this.btn_CheckData.Click += new System.EventHandler(this.btn_CheckData_Click);
            // 
            // dgv_Preview
            // 
            this.dgv_Preview.AllowUserToAddRows = false;
            this.dgv_Preview.AllowUserToDeleteRows = false;
            this.dgv_Preview.AllowUserToResizeRows = false;
            this.dgv_Preview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_Preview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Preview.Location = new System.Drawing.Point(3, 19);
            this.dgv_Preview.Name = "dgv_Preview";
            this.dgv_Preview.ReadOnly = true;
            this.dgv_Preview.RowHeadersVisible = false;
            this.dgv_Preview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Preview.Size = new System.Drawing.Size(529, 118);
            this.dgv_Preview.TabIndex = 4;
            // 
            // dgv_NeedToFix
            // 
            this.dgv_NeedToFix.AllowUserToAddRows = false;
            this.dgv_NeedToFix.AllowUserToDeleteRows = false;
            this.dgv_NeedToFix.AllowUserToResizeRows = false;
            this.dgv_NeedToFix.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_NeedToFix.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_NeedToFix.Location = new System.Drawing.Point(3, 19);
            this.dgv_NeedToFix.Name = "dgv_NeedToFix";
            this.dgv_NeedToFix.ReadOnly = true;
            this.dgv_NeedToFix.RowHeadersVisible = false;
            this.dgv_NeedToFix.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgv_NeedToFix.Size = new System.Drawing.Size(529, 118);
            this.dgv_NeedToFix.TabIndex = 5;
            this.dgv_NeedToFix.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgv_NeedToFix_RowPrePaint);
            // 
            // btn_Export
            // 
            this.btn_Export.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Export.Location = new System.Drawing.Point(3, 403);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.Size = new System.Drawing.Size(175, 23);
            this.btn_Export.TabIndex = 6;
            this.btn_Export.Text = "Export new csv files - fix";
            this.btn_Export.UseVisualStyleBackColor = true;
            this.btn_Export.Click += new System.EventHandler(this.btn_Export_Click);
            // 
            // lbl_Hint
            // 
            this.lbl_Hint.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl_Hint, 2);
            this.lbl_Hint.ForeColor = System.Drawing.Color.Brown;
            this.lbl_Hint.Location = new System.Drawing.Point(3, 0);
            this.lbl_Hint.Name = "lbl_Hint";
            this.lbl_Hint.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lbl_Hint.Size = new System.Drawing.Size(28, 18);
            this.lbl_Hint.TabIndex = 7;
            this.lbl_Hint.Text = "hint";
            // 
            // btn_ClearItems
            // 
            this.btn_ClearItems.Location = new System.Drawing.Point(3, 3);
            this.btn_ClearItems.Name = "btn_ClearItems";
            this.btn_ClearItems.Size = new System.Drawing.Size(175, 23);
            this.btn_ClearItems.TabIndex = 8;
            this.btn_ClearItems.Text = "Clear items";
            this.btn_ClearItems.UseVisualStyleBackColor = true;
            this.btn_ClearItems.Click += new System.EventHandler(this.btn_ClearItems_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Selected file, preview:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Need to fix:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Desktop\\CSV_PRICE_FIX\\";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_Hint, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(730, 456);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_ClearItems);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btn_ImportFiles);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Controls.Add(this.btn_CheckData);
            this.panel1.Controls.Add(this.chklb_FieldsToCheck);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 21);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 3);
            this.panel1.Size = new System.Drawing.Size(183, 432);
            this.panel1.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Brown;
            this.label5.Location = new System.Drawing.Point(3, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Fields to check:";
            // 
            // chklb_FieldsToCheck
            // 
            this.chklb_FieldsToCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chklb_FieldsToCheck.BackColor = System.Drawing.SystemColors.Control;
            this.chklb_FieldsToCheck.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chklb_FieldsToCheck.CheckOnClick = true;
            this.chklb_FieldsToCheck.FormattingEnabled = true;
            this.chklb_FieldsToCheck.Location = new System.Drawing.Point(3, 99);
            this.chklb_FieldsToCheck.Name = "chklb_FieldsToCheck";
            this.chklb_FieldsToCheck.Size = new System.Drawing.Size(175, 272);
            this.chklb_FieldsToCheck.TabIndex = 17;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.listBox1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(192, 21);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(535, 140);
            this.panel4.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Source files loaded:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.dgv_Preview);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(192, 167);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(535, 140);
            this.panel2.TabIndex = 14;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.dgv_NeedToFix);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(192, 313);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(535, 140);
            this.panel3.TabIndex = 15;
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(754, 481);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(770, 520);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ebay uploads(.csv) validator v1.11";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Preview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_NeedToFix)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

 
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_ImportFiles;
        private System.Windows.Forms.Button btn_CheckData;
        private System.Windows.Forms.DataGridView dgv_Preview;
        private System.Windows.Forms.DataGridView dgv_NeedToFix;
        private System.Windows.Forms.Button btn_Export;
        private System.Windows.Forms.Label lbl_Hint;
        private System.Windows.Forms.Button btn_ClearItems;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckedListBox chklb_FieldsToCheck;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
    }
}

