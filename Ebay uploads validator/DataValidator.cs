﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ebay_uploads_validator
{
    class Records
    {
        public AbsEbay.AbsEbay.DataBaseAccName account { get; set; }
        public char ai { get; set; }
        //public List<CheckIdResult> checkResults { get; set; }
    }

    //class CheckIdResult
    //{
    //    public string id { get; set; }
    //    public bool success { get; set; }
    //}

    class DataValidator
    {
        Records _recs;
        DataTable _result;
        DataTable _source;
        List<KeyValuePair<string, string>> _fieldsToCheck;
        
        private DataTable _checkResultProperties;
        public DataTable CheckResultProperties
        {
            get { return _checkResultProperties; }
            set { _checkResultProperties = value; }
        }

        public static List<KeyValuePair<string, string>> FieldsToCheck = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("price", "Price"),
            new KeyValuePair<string, string>("title", "Title"),
            new KeyValuePair<string, string>("parsed_shipping", "ShippingService-1:Cost"),
            new KeyValuePair<string, string>("ht", "DispatchTimeMax"),
            new KeyValuePair<string, string>("qty", "Quantity"),
        };

        public DataValidator(DataTable dt, List<KeyValuePair<string, string>> fieldsToCheck)
        {
            _source = dt;
            _result = dt.Clone();
            _fieldsToCheck = fieldsToCheck;
            _recs = new Records();
            _recs.account = AbsEbay.AbsEbay.DataBaseAccName.None;

            foreach (AbsEbay.AbsEbay.DataBaseAccName item in Enum.GetValues(typeof(AbsEbay.AbsEbay.DataBaseAccName)))
            {
                if (item == AbsEbay.AbsEbay.DataBaseAccName.None) continue;

                if (Regex.IsMatch(dt.TableName, string.Format("(?i){0}_a", item.ToString())))
                {
                    _recs.account = item;
                    _recs.ai = 'a';
                    break;
                }
                else if (Regex.IsMatch(dt.TableName, string.Format("(?i){0}_i", item.ToString())))
                {
                    _recs.account = item;
                    _recs.ai = 'i';
                    break;
                }
            }

            if (_recs.account == AbsEbay.AbsEbay.DataBaseAccName.None)
                throw new Exception("Can't recognize account from file name!");
        }

        public DataTable CheckRevise()
        {
            List<string> p_strings = new List<string>();
            foreach (DataRow rw in _source.Rows)
            {
                p_strings.Add(
                    string.Format("('{0}','{1}','{2}','{3}',{4},{5},{6},{7})"
                        , _recs.account.ToString()
                        , _recs.ai
                        , (_recs.ai == 'a') ? rw["ItemID"].ToString() : rw["TemplateID"].ToString()
                        , (rw.Table.Columns.Contains("Title")) ? rw["Title"].ToString() : string.Empty
                        , (rw.Table.Columns.Contains("Price")) ? (rw["Price"].ToString() == string.Empty) ? "null": rw["Price"].ToString() : "null"
                        , (rw.Table.Columns.Contains("ShippingService-1:Cost")) ? (rw["ShippingService-1:Cost"].ToString() == string.Empty) ? "null" : rw["ShippingService-1:Cost"].ToString() : "null"
                        , (rw.Table.Columns.Contains("Quantity")) ? (rw["Quantity"].ToString() == string.Empty) ? "null" : rw["Quantity"].ToString() : "null"
                        , (rw.Table.Columns.Contains("DispatchTimeMax")) ? (rw["DispatchTimeMax"].ToString() == string.Empty) ? "null" : rw["DispatchTimeMax"].ToString() : "null"
                    )
                );
            }

            string q =
@"create temporary table temp_tbl (
    acc text,
    ai bpchar,
    id varchar(15),
    title text,
    price numeric(14,2),
    parsed_shipping numeric(14,2),
    qty int4,
    ht int4
) on commit drop;
insert into temp_tbl (acc, ai, id, title, price, parsed_shipping, qty, ht) values " + string.Join(",", p_strings) + @";

with qq as (
    -- get only existing id's
    select t0.*
    from 
    temp_tbl t0 join 
    tdr.all_accs t1 on
            t0.acc = t1.acc
        and t0.ai = t1.ai
        and t0.id = t1.id
)
select distinct qq.*,t2.title title_curr, t2.price price_curr, t2.parsed_shipping parsed_shipping_curr, t2.qty qty_curr, t2.ht ht_curr 
from qq left join 
tdr.all_accs t1 on
        qq.acc = t1.acc
    and qq.ai = t1.ai
    and qq.id = t1.id";

            foreach (var item in _fieldsToCheck)
                q += string.Format("{1}    and qq.{0} = t1.{0}", item.Key, Environment.NewLine);

            q += 
@" join
tdr.all_accs t2 on
        qq.acc = t2.acc
    and qq.ai = t2.ai
    and qq.id = t2.id";

            q += " where t1.id is null;";

            NpgsqlConnection conn = new NpgsqlConnection("Server=192.168.1.5;Port=5432;User Id=tdr;Password=^_^;Database=autobodyshop;pooling=false;");
            NpgsqlCommand cmd = new NpgsqlCommand(q, conn);

            Debug.WriteLine(string.Join(",", p_strings));
            DataTable dtResponse = new DataTable();
            try
            {
                conn.Open();
                NpgsqlDataReader dr = cmd.ExecuteReader();
                dtResponse.Load(dr);
                conn.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            // save source rows matched by id with query response rows to _result DataTable
            foreach (DataRow item in dtResponse.Rows)
                _source.AsEnumerable().Where(row => row[(_recs.ai == 'a') ? "ItemID" : "TemplateID"].ToString() == item["id"].ToString())
                    .ToList()
                    .ForEach(row => _result.ImportRow(row));

            CheckResultProperties = new DataTable(_result.TableName);
            CheckResultProperties.Columns.Add("id", typeof(string));
            CheckResultProperties.Columns.Add("field", typeof(string));

            // get list of ids to store from result table
            // other ids should be abandoned
            List<string> IdsToKeep = new List<string>();
            foreach (DataRow rw in dtResponse.Rows)
            {
                string id = rw["id"].ToString();
                foreach (KeyValuePair<string,string> field in _fieldsToCheck)
                {
                    if (!string.IsNullOrWhiteSpace(rw[field.Key].ToString()) && rw[field.Key].ToString() != rw[field.Key + "_curr"].ToString())
                    {
                        IdsToKeep.Add(id);
                        DataRow r = CheckResultProperties.NewRow();
                        r["id"] = id;
                        r["field"] = field.Value;
                        CheckResultProperties.Rows.Add(r);
                    }
                }
            }

            // get final result by ids
            DataTable _resultFinal = _result.Clone();
            _result.AsEnumerable().Where(x => IdsToKeep.Contains(x.Field<string>((_recs.ai == 'a') ? "ItemID" : "TemplateID")))
                .ToList()
                .ForEach(row => _resultFinal.ImportRow(row));

            return (_resultFinal.Rows.Count > 0) ? _resultFinal : new DataTable(_resultFinal.TableName);
        }
    }
}
