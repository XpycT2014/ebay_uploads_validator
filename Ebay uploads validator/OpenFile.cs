﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.VisualBasic.FileIO;

namespace Ebay_uploads_validator
{
    class OpenFile
    {
        public static DataTable ExcelToTable(string filePath, string sheet = "Sheet1", bool firstRowAsHeader = false)
        {
            string hdr;
            if (firstRowAsHeader)
            {
                hdr = "Yes";
            }
            else
            {
                hdr = "No";
            }

            var dt = new DataTable();
            if (File.Exists(filePath))
            {
                string con;
                if (filePath.Substring(filePath.LastIndexOf(".") + 1).ToString() == "xls")
                {
                    con =
                    @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath +
                    @";Extended Properties='Excel 8.0;HDR=" + hdr + ";IMEX=1;'";
                }
                else
                {
                    con =
                    @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath +
                    @";Extended Properties='Excel 12.0;HDR=" + hdr + ";IMEX=1;'";
                }

                using (OleDbConnection connection = new OleDbConnection(con))
                {
                    //var dt = new DataTable();
                    connection.Open();
                    OleDbCommand command =
                        new OleDbCommand("select * from [" + sheet + "$]", connection);
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i).Trim(), typeof(string));
                        }
                        while (dr.Read())
                        {
                            DataRow row = dt.NewRow();
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                row[i] = dr[i].ToString();
                            }
                            dt.Rows.Add(row);
                        }
                    }
                }
            }

            return dt;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="separator"></param>
        /// <param name="hasHeader"></param>
        /// <param name="encoding">"ISO 8859-1" - for de || "UTF8" - for com and uk</param>
        /// <returns></returns>
        public static DataTable CsvToTable(string filePath, string separator, bool hasHeader = false, string encoding = "UTF8") 
        {
            DataTable result = new DataTable();
            if (System.IO.File.Exists(filePath))
            {
                // Encoding.GetEncoding(28591),  ISO 8859-1 - for de
                // Encoding.UTF8 - for com and uk
                TextFieldParser parser;
                if (encoding == "UTF8")
                {
                     parser = new TextFieldParser(filePath, Encoding.UTF8);
                }
                else
                {
                     parser = new TextFieldParser(filePath, Encoding.GetEncoding(28591));
                }
                
                parser.Delimiters = new string[] { separator };
                parser.HasFieldsEnclosedInQuotes = true;
                //use if data may contain delimiters
                parser.TextFieldType = FieldType.Delimited;
                parser.TrimWhiteSpace = true;
                bool headerFlag = false;
                if (hasHeader)
                {
                    headerFlag = true;
                }
                while (!parser.EndOfData)
                {
                    string[] a = parser.ReadFields();
                    //if (AddValuesToTable(parser.ReadFields(), result, HeaderFlag))
                    if (AddValuesToTable(ref a, result, headerFlag))
                    {
                        headerFlag = false;
                    }
                    else
                    {
                        parser.Close();
                        return null;
                    }
                }
                parser.Close();
                return result;
            }
            else
            {
                return null;
            }
        }

        private static bool AddValuesToTable(ref string[] source, DataTable destination, bool headerFlag)
        {
            //Ensures a datatable can hold an array of values and then adds a new row
            try
            {
                int existing = destination.Columns.Count;
                if (headerFlag)
                {
                    Resolve_Duplicate_Names(ref source);
                    for (int i = 0; i <= source.Length - existing - 1; i++)
                    {
                        destination.Columns.Add(source[i], typeof(string));
                    }
                    return true;
                }
                for (int i = 0; i <= source.Length - existing - 1; i++)
                {
                    string colName = (existing + 1 + i).ToString();
                    destination.Columns.Add("Column", typeof(string));
                }
                destination.Rows.Add(source);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        private static void Resolve_Duplicate_Names(ref string[] source)
        {
            // Resolves the possibility of duplicated names by appending "Duplicate Name" and a number at the end of any duplicates
            int i = 0;
            int n = 0;
            int dnum = 0;
            dnum = 1;
            for (n = 0; n <= source.Length - 1; n++)
            {
                for (i = n + 1; i <= source.Length - 1; i++)
                {
                    if (source[i] == source[n])
                    {
                        source[i] = source[i] + "Duplicate Name " + dnum;
                        dnum += 1;
                    }
                }
            }
        }

    }
}