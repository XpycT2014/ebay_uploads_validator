﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Ebay_uploads_validator
{
    public partial class MainForm : Form
    {
        private List<string> _fileNames = new List<string>();
        event EventHandler FileNames_Changed;
        public List<string> FileNames
        {
            get { return _fileNames; }
            set
            {
                _fileNames = value;
                if (FileNames_Changed != null)
                    FileNames_Changed(_fileNames, new EventArgs());
            }
        }

        DataSet dsTablesToCheck = new DataSet();
        DataSet dsCheckResults = new DataSet();
        DataSet dsCheckResultsProperties = new DataSet();

        public MainForm()
        {
            InitializeComponent();

            Text = string.Format("{0} v{1}", Application.ProductName, Application.ProductVersion);

            listBox1.AllowDrop = true;
            listBox1.HorizontalScrollbar = true;
            btn_CheckData.Enabled = false;
            btn_Export.Enabled = false;

            // fill hint label
            StringBuilder sb = new StringBuilder("File names must contain following acc names:\n");
            List<string> tmp = new List<string>();
            foreach (AbsEbay.AbsEbay.DataBaseAccName item in Enum.GetValues(typeof(AbsEbay.AbsEbay.DataBaseAccName)))
                if (item != AbsEbay.AbsEbay.DataBaseAccName.None)
                    tmp.Add(string.Format("{0}_a, {0}_i", item.ToString()));

            sb.Append(string.Join(", ", tmp));
            lbl_Hint.Text = sb.ToString();

            //fill fields to check CheckedListBox
            ((ListBox)chklb_FieldsToCheck).DataSource = DataValidator.FieldsToCheck;
            ((ListBox)chklb_FieldsToCheck).DisplayMember = "Value";
            ((ListBox)chklb_FieldsToCheck).ValueMember = "Key";
            chklb_FieldsToCheck.SetItemChecked(0, true);

            FileNames_Changed += On_FileNames_Changed;
        }

        void On_FileNames_Changed(object sender, EventArgs e)
        {
            listBox1.DataSource = FileNames;
            listBox1.Refresh();

            dsTablesToCheck = new DataSet();
            dsCheckResults = new DataSet();
            dsCheckResultsProperties = new DataSet();

            dgv_Preview.DataSource = null;
            dgv_NeedToFix.DataSource = null;

            btn_CheckData.Enabled =
                btn_Export.Enabled = (FileNames.Count > 0) ? true : false;

        }

        void btn_ClearItems_Click(object sender, EventArgs e)
        {
            FileNames = new List<string>();
        }

        void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        void listBox1_DragDrop(object sender, DragEventArgs e)
        {
            string[] filepaths = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            List<string> newList = new List<string>();
            newList = listBox1.Items.Cast<string>().ToList();
            foreach (string filepath in filepaths)
                newList.AddRange(GetAllFiles(filepath));

            FileNames = newList.Distinct().ToList();
        }

        public static string[] GetAllFiles(string directory)
        {
            if (File.GetAttributes(directory) == FileAttributes.Directory)
            {
                return Directory.GetFiles(directory, "*.csv", SearchOption.AllDirectories);
            }
            else
            {
                return Regex.IsMatch(directory, "(?i)csv$") ? new string[] { directory } : new string[] { };
            }
        }

        void btn_ImportFiles_Click(object sender, EventArgs e)
        {
            List<string> newList = new List<string>();
            newList = listBox1.Items.Cast<string>().ToList();
            openFileDialog1.Multiselect = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (string fileName in openFileDialog1.FileNames)
                {
                    if (Regex.IsMatch(fileName, "(?i)csv$"))
                        newList.Add(fileName);
                }
            }
            FileNames = newList.Distinct().ToList();
        }

        void FillDataSetFromFile(string fileName, ref DataSet ds)
        {
            FileInfo fi = new FileInfo(fileName);
            if (IsFileNameOk(fi.FullName))
            {
                DataTable dt = new DataTable();
                dt = OpenFile.CsvToTable(fileName, ",", true);
                dt.TableName = fi.FullName;
                ds.Tables.Add(dt);
            }
        }

        bool IsFileNameOk(string fileName)
        {
            bool match_found = false;
            foreach (var item in Enum.GetValues(typeof(AbsEbay.AbsEbay.DataBaseAccName)))
            {
                string pattern = string.Format("(?i){0}_a|(?i){0}_i", item.ToString());
                match_found = Regex.IsMatch(fileName, pattern);
                if (match_found) break;
            }
            return match_found;
        }

        void btn_CheckData_Click(object sender, EventArgs e)
        {
            if (FileNames.Count > 0)
            {
                btn_Export.Enabled = false;
                listBox1.Refresh();
                dsTablesToCheck = new DataSet();
                dsCheckResults = new DataSet();
                dsCheckResultsProperties = new DataSet();


                foreach (string fileName in listBox1.Items)
                    FillDataSetFromFile(fileName, ref dsTablesToCheck);

                //get selected fields to perform check with
                List<KeyValuePair<string, string>> sel = new List<KeyValuePair<string, string>>();
                foreach (KeyValuePair<string, string> item in chklb_FieldsToCheck.CheckedItems)
                    sel.Add(new KeyValuePair<string, string>(item.Key, item.Value));

                if (sel.Count == 0)
                {
                    MessageBox.Show("Nothing to do! No fields to check selected.");
                    return;
                }

                foreach (DataTable dt in dsTablesToCheck.Tables)
                {
                    DataValidator dv = new DataValidator(dt, sel);
                    DataTable dtResult = dv.CheckRevise();
                    dtResult.TableName = dt.TableName;
                    dsCheckResults.Tables.Add(dtResult);
                    dsCheckResultsProperties.Tables.Add(dv.CheckResultProperties);
                }

                btn_Export.Enabled = true;
                MessageBox.Show("all files checked, now you can preview results & export all.");
            }
            else
            {
                MessageBox.Show("Nothing to do! Please import files to check.");
            }
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (listBox1.Items.Count > 0)
            {
                btn_CheckData.Enabled = true;
                string s = (string)listBox1.Items[listBox1.SelectedIndex];

                if (dsTablesToCheck.Tables.Contains(s))
                    dgv_Preview.DataSource = dsTablesToCheck.Tables[s];

                if (dsCheckResults.Tables.Contains(s))
                    dgv_NeedToFix.DataSource = dsCheckResults.Tables[s];
            }
        }

        private void btn_Export_Click(object sender, EventArgs e)
        {
            foreach (DataTable tbl in dsCheckResults.Tables)
            {
                if (tbl.Rows.Count > 0)
                {
                    FileInfo fi = new FileInfo(tbl.TableName);
                    string filePath = Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory) + @"\CSV_PRICE_FIX\fix_" + DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss") + "_" + fi.Name;
                    string content = Helper.CsvFile.TableToCsv(tbl, ',');
                    using (StreamWriter writer = new StreamWriter(filePath)) writer.WriteLine(content);
                }
            }
            MessageBox.Show("Jobs done.");
        }

        private static string DataTableToCSV(DataTable datatable, string seperator = ",")
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < datatable.Columns.Count; i++)
            {
                sb.Append(datatable.Columns[i]);
                if (i < datatable.Columns.Count - 1)
                    sb.Append(seperator);
            }
            sb.AppendLine();
            foreach (DataRow dr in datatable.Rows)
            {
                for (int i = 0; i < datatable.Columns.Count; i++)
                {
                    sb.Append(dr[i].ToString());

                    if (i < datatable.Columns.Count - 1)
                        sb.Append(seperator);
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }

        private void dgv_NeedToFix_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (listBox1.Items.Count > 0)
            {
                string s = (string)listBox1.Items[listBox1.SelectedIndex];
                if (dsCheckResultsProperties.Tables.Contains(s))
                {
                    string idOrTemplateId = dgv_NeedToFix.Columns.Contains("ItemID") ? "ItemID" : "TemplateID";
                    FormatRow(dgv_NeedToFix.Rows[e.RowIndex], dsCheckResultsProperties.Tables[s], idOrTemplateId);
                }
            }
        }

        private void FormatRow(DataGridViewRow row, DataTable ids, string idOrItemId)
        {
            List<DataRow> rows = ids.AsEnumerable().Where(r => r.Field<string>("id") == row.Cells[idOrItemId].Value.ToString()).ToList();
            foreach (DataRow item in rows)
                row.Cells[item.Field<string>("field")].Style.ForeColor = Color.Red;
        }
    }
}